#!/bin/bash

cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat system_ext/app/FaceID/FaceID.apk.* 2>/dev/null >> system_ext/app/FaceID/FaceID.apk
rm -f system_ext/app/FaceID/FaceID.apk.* 2>/dev/null
cat system_ext/app/HealthLife/HealthLife.apk.* 2>/dev/null >> system_ext/app/HealthLife/HealthLife.apk
rm -f system_ext/app/HealthLife/HealthLife.apk.* 2>/dev/null
cat system_ext/app/TranssionCamera/TranssionCamera.apk.* 2>/dev/null >> system_ext/app/TranssionCamera/TranssionCamera.apk
rm -f system_ext/app/TranssionCamera/TranssionCamera.apk.* 2>/dev/null
cat system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk.* 2>/dev/null >> system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk
rm -f system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk.* 2>/dev/null
